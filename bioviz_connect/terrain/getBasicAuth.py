import os
import requests
from .. import settings
import sys

##
{
    'access_token': 'AT-2910-X98kCFohoh7IgKr54qsMe23C9wc0yDso',
    'expires_in': '57600'
}
##


def getBasicAuth():
    uname = settings.USERNAME
    passwd = settings.PASSWORD
    req_url = settings.BASE_URL + '/token'
    r = requests.get(req_url, auth=(uname, passwd))
    settings.ACCESS_TOKEN = r.json()['access_token']
    rooturl = settings.ROOT_URL
    redirect_url = rooturl + '/cyverseData'
    return r.json()['access_token']

