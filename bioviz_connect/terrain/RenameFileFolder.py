import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def rename_FileFolder(source, dest, redis_sessiondata):
    try:
        accesstoken = str(redis_sessiondata[0])
        request = RequestCreator.renameFileFolder_request(source, dest)
        req_url = 'https://de.cyverse.org/terrain/secured/filesystem/rename'
        r = requests.post(req_url, headers={'Accept': 'application/json',
                                        'Authorization': 'BEARER ' + accesstoken,
                                        'Content-type': 'text/plain'},
                      data=request)

        json_data = r.json()

        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_EXISTS'):
                raise FileExistsError()

        return json_data
    except PermissionError as error:
        raise PermissionError()
    except Exception as error:
        return Exception()


