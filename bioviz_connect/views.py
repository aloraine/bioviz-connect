import json

from .terrain.moveFileFolder import move_FileFolder
from . import settings
from django.http import HttpResponse, HttpResponseRedirect
from .getSpeciesVersionList import getSpeciesVersionList
from .terrain.getUserData import getUserData
from .terrain.getSharedData import getSharedData
from .terrain.getCommunityData import getCommunityData
from .terrain.getUserDetail import getUserDetails
from .terrain.getUserDetail import getUserDetailJson, getSectionFromRootData
from .terrain.searchForFile import searchForFile
from .terrain.searchUser import searchForUser
from .terrain.getMetadata import getMetadata
from .terrain.saveMetadata import saveMetadata
from .terrain.shareFile import shareFile
from .terrain.unshareFile import unshareFile
from .terrain.checkFilePermissions import checkFilePermissions, checkFilePermissions_search
from .terrain.getSessionUserData import getSessionIdfromCookie
from .terrain.getSessionUserData import getaccesstokenfromredis
from .terrain.getSessionUserData import getUserNamefromSessionData
from .terrain.getSessionUserData import getCommunityData_Prefix
from .terrain.getSessionUserData import getHomePath_Prefx, gethomePathfromSessionData
from .terrain.getAnalysisHistory import getAnalysisHistory
from .terrain.casLogin import casLogin
from .terrain.runCoverageGraphAnalysis import runRestCoverageGraphAnalysis
from .terrain.runInitializeUserWorkspace import runInitializeUserWorkspace
from django.shortcuts import render
from .terrain.getAccessToken import getAccessToken
from .terrain.getBasicAuth import getBasicAuth
from .terrain.ManageAppDetails import *
from .terrain.ManageAppDetails import getHtmlDataForAppId, getNameForApp
from .terrain.createFolder import createDirectory
from .terrain.RenameFileFolder import rename_FileFolder
from .terrain.UploadFileRequest import upload_File
from .terrain.uploadFileByURL import uploadFileBy_Url
from .terrain_model.ResponseParser import ResponseParser
from .terrain_model.CustomEncoder import CustomEncoder
from .terrain.deleteFileFolder import delete_FileFolder
import requests
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache
import uuid
import sys
from django.utils.timezone import utc
import datetime

def index(request):
    context = {}
    return render(request, 'index.html', context)


def home(request):
    context = {}
    return render(request, 'bioviz_connect/middlepanel.html', context)


def saveSessionRedis(accesstoken):
    session_id = str(uuid.uuid4())
    userdata = getUserDetailJson(accesstoken)
    cache.set(session_id, [accesstoken, userdata], timeout=settings.REDIS_EXPIRY)
    return session_id

def setcookie(request):
    now = datetime.datetime.utcnow().replace(tzinfo=utc)
    response = HttpResponse("cookie-warning-date")
    response.set_cookie(key='cookie-warning-date', value=now.strftime("%y-%m-%d %H:%M:%S"));
    return response

def getcookie(request):
    now = datetime.datetime.utcnow()
    lastdate  = request.COOKIES.get('cookie-warning-date','default')
    if(lastdate=='default'):
        return HttpResponse(True);
    else:
        lastdate = datetime.datetime.strptime(lastdate,"%y-%m-%d %H:%M:%S")
        diff = now - lastdate;
        if(diff.total_seconds()>=365*3600*24):
            return HttpResponse(True)
        else:
            return HttpResponse(False)


def loadlogin(request):
    context = {}
    # print("Development Environment: "+settings.IS_DEV_ENVIRONMENT)
    if settings.IS_DEV_ENVIRONMENT == 'True':
        accesstoken = getBasicAuth()
        session_id = saveSessionRedis(accesstoken)
        response = HttpResponseRedirect('middlepanel/')
        response.set_cookie(key='sessionId', value=session_id)
        return response
    return render(request, 'login.html', context)


def login(request):
    if request.is_ajax():
        try:
            redirectUrl = casLogin()
        except:
            e = sys.exc_info()
            return HttpResponse(e)
    return HttpResponse(redirectUrl, content_type='text/plain')


def loginCyverse(request):
    userData = getUserData('', '20', '0', 'NAME', 'ASC', '', '')
    context = {
        "userData": userData
    }
    return render(request, 'base.html', context)


def oauth(request):
    context = {}
    return render(request, 'oauth.html', context)


def getSessionId(request):
    code = request.GET.get('code')
    access_token = getAccessToken(code)
    sessionId = saveSessionRedis(access_token)
    redis_sessiondata = getSessionData(sessionId)
    user_name = getUserNamefromSessionData(redis_sessiondata)
    savedPath = cache.get(user_name)
    if (savedPath):
        cache.delete(user_name)
    response = HttpResponse(savedPath, content_type='text/plain')
    response.set_cookie(key='sessionId', value=sessionId)
    return response


def getSessionData(sessionid):
    try:
        sessionData = cache.get(sessionid)
        if not sessionData:
            raise LookupError()
        return sessionData
    except LookupError as e:
        raise LookupError()


def base(request):
    context = {}
    return render(request, 'middlepanel.html', context)

@csrf_exempt
def middlepanel(request):
    response = None
    if request.is_ajax():
        try:
            if request.method == "POST":
                 data_attrib = json.loads(request.body)
                 if 'redirect_savedurl' in data_attrib:
                     redirect_savedurl = str(data_attrib['redirect_savedurl'])
                 else:
                     redirect_savedurl = ''

                 path = str(data_attrib['url'])
                 limit = str(data_attrib['limit'])
                 offset = str(data_attrib['offset'])
                 sortcol = str(data_attrib['sortcol'])
                 sortdir = str(data_attrib['sortdir'])
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            user_name = getUserNamefromSessionData(redis_sessiondata)
            response = getUserData(path, limit, offset, sortcol, sortdir, redis_sessiondata)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

@csrf_exempt
def renderSharedData(request):
    response = None
    if request.is_ajax():
        try:
            if request.method == "POST":
                 data_attrib = json.loads(request.body)
                 if 'redirect_savedurl' in data_attrib:
                     redirect_savedurl = str(data_attrib['redirect_savedurl'])
                 else:
                     redirect_savedurl = ''

                 path = str(data_attrib['url'])
                 limit = str(data_attrib['limit'])
                 offset = str(data_attrib['offset'])
                 sortcol = str(data_attrib['sortcol'])
                 sortdir = str(data_attrib['sortdir'])

            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            user_name = getUserNamefromSessionData(redis_sessiondata)
            response = getSharedData(path, limit, offset, sortcol, sortdir, redis_sessiondata)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def getFileIdApi(request):
    response = None
    if request.is_ajax():
        try:
            path = request.GET.get('url')
            if path is None:
                raise Exception("The request header does not contain Path")
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            response = getMetadata(path, accesstoken)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)

@csrf_exempt
def retrieveMetaData(request):
    response = None

    if request.is_ajax():
        try:
            data_attrib = json.loads(request.body)
            if 'redirect_savedurl' in data_attrib:
                redirect_savedurl = str(data_attrib['redirect_savedurl'])
            else:
                redirect_savedurl = ''

            fileId = str(data_attrib['fileId'])
            if fileId is None:
                raise Exception("The request header does not contain fileId")
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            user_name = getUserNamefromSessionData(redis_sessiondata)

            if 'current_section_search' in data_attrib:
                current_section_search = str(data_attrib['current_section_search'])
            else:
                current_section_search = getSectionFromRootData(user_name, redirect_savedurl)

            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            response = getMetadata(fileId, accesstoken)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            redirect_savedurl = redirect_savedurl + "?currentSection="+current_section_search

            cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)


@csrf_exempt
def saveMetaData(request):

    response = None
    if request.is_ajax():
        try:
            fileId = request.POST['fileId']
            data_attrib = json.loads(request.POST['data'])
            if fileId is None:
                raise Exception("The request header does not contain fileId")
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            response = saveMetadata(fileId, data_attrib, accesstoken)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)

@csrf_exempt
def getCommunityDataList(request):
    response = None
    if request.is_ajax():
        try:
            if request.method == "POST":
                 data_attrib = json.loads(request.body)

                 if 'redirect_savedurl' in data_attrib:
                     redirect_savedurl = str(data_attrib['redirect_savedurl'])
                 else:
                     redirect_savedurl = ''

                 path = str(data_attrib['url'])
                 limit = str(data_attrib['limit'])
                 offset = str(data_attrib['offset'])
                 sortcol = str(data_attrib['sortcol'])
                 sortdir = str(data_attrib['sortdir'])

            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            user_name = getUserNamefromSessionData(redis_sessiondata)
            response = getCommunityData(path, limit, offset, sortcol, sortdir, redis_sessiondata)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

@csrf_exempt
def setPublicLink(request):
    response = None
    if request.is_ajax():
        try:
            data_attrib = json.loads(request.body)
            path = str(data_attrib['path'])
            is_anonymous = data_attrib['is_anonymous']
            if 'redirect_savedurl' in data_attrib:
                redirect_savedurl = str(data_attrib['redirect_savedurl'])
            else:
                redirect_savedurl = ''

            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            user_name = getUserNamefromSessionData(redis_sessiondata)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            username = getUserNamefromSessionData(redis_sessiondata)
            if (bool(is_anonymous)):
                username = "anonymous"
            response = shareFile(path, username, accesstoken)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

@csrf_exempt
def getRootPathUser(request):
    response = None
    if request.is_ajax():
        try:
            data_attrib = json.loads(request.body)
            if 'redirect_savedurl' in data_attrib:
                redirect_savedurl = str(data_attrib['redirect_savedurl'])
            else:
                redirect_savedurl = ''

            if 'currentSection' in data_attrib:
                currentSection = str(data_attrib['currentSection'])
            else:
                currentSection = ''

            sessionId = getSessionIdfromCookie(request.COOKIES)
            redis_data = getSessionData(sessionId)
            user_name = getUserNamefromSessionData(redis_data)

            accesstoken = getaccesstokenfromredis(redis_data)
            response = getUserDetails(accesstoken)
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            currentSection = getSectionFromRootData(user_name, redirect_savedurl)
            redirect_savedurl = redirect_savedurl + "?currentSection="+currentSection
            cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(response)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

@csrf_exempt
def searchFile(request):
    response = None
    if request.is_ajax():
        try:
            if request.method == "POST":
                 data_attrib = json.loads(request.body)

                 if 'redirect_savedurl' in data_attrib:
                     redirect_savedurl = str(data_attrib['redirect_savedurl'])
                 else:
                     redirect_savedurl = ''

                 size = int(data_attrib['limit'])
                 offset = int(data_attrib['offset'])
                 sort_field = str(data_attrib['sortcol'])
                 sort_order = str(data_attrib['sortdir'])
                 label = str(data_attrib['label'])
                 type = str(data_attrib['type'])
                 if 'exact' in data_attrib:
                     exact = bool(str(data_attrib['exact']))
                 else:
                     exact = False


            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            username = getUserNamefromSessionData(redis_sessiondata)

            if 'current_section_search' in data_attrib:
                current_section_search = str(data_attrib['current_section_search'])
            else:
                current_section_search = getSectionFromRootData(username, redirect_savedurl)

            home_path_prefix = getHomePath_Prefx(username, redis_sessiondata)
            community_path_prefix = getCommunityData_Prefix(username, redis_sessiondata)
            response = searchForFile(exact, label, size, type, offset, sort_field, sort_order, accesstoken, username, home_path_prefix, community_path_prefix)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            redirect_savedurl = redirect_savedurl + "?currentSection="+current_section_search
            cache.set(username, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

@csrf_exempt
def remPublicLink(request):
    response = None
    if request.is_ajax():
        try:
            data_attrib = json.loads(request.body)
            path = str(data_attrib['path'])
            is_anonymous = data_attrib['is_anonymous']
            if 'redirect_savedurl' in data_attrib:
                redirect_savedurl = str(data_attrib['redirect_savedurl'])
            else:
                redirect_savedurl = ''

            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            username = getUserNamefromSessionData(redis_sessiondata)
            if (bool(is_anonymous)):
                username = "anonymous"
            response = unshareFile(path, username, accesstoken)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            cache.set(username, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

@csrf_exempt
def checkFilePermission(request):
    response = None
    if request.is_ajax():
        try:
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            user_name = getUserNamefromSessionData(redis_sessiondata)

            data_attrib = json.loads(request.body)
            if 'redirect_savedurl' in data_attrib:
                redirect_savedurl = str(data_attrib['redirect_savedurl'])
            else:
                redirect_savedurl = ''

            path = str(data_attrib['path'])
            response = checkFilePermissions(path, redis_sessiondata)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401
            cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        context = {}
        return render(request, 'middlepanel.html', context)

@csrf_exempt
def checkFilePermission_Search(request):
    response = None
    if request.is_ajax():
        try:
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            user_name = getUserNamefromSessionData(redis_sessiondata)

            data_attrib = json.loads(request.body)
            if 'redirect_savedurl' in data_attrib:
                redirect_savedurl = str(data_attrib['redirect_savedurl'])
            else:
                redirect_savedurl = ''

            if 'current_section_search' in data_attrib:
                current_section_search = str(data_attrib['current_section_search'])
            else:
                current_section_search = getSectionFromRootData(user_name, redirect_savedurl)

            path = str(data_attrib['path'])
            response = checkFilePermissions_search(path, redis_sessiondata)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except PermissionError as e:
            response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 401

            redirect_savedurl = redirect_savedurl + "?currentSection="+current_section_search

            cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        context = {}
        return render(request, 'middlepanel.html', context)



def searchtheUser(request):
    response = None
    if request.is_ajax():
        try:
            label = request.GET.get('label')
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            response = searchForUser(label, accesstoken)
        except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def getSpeciesVersion(request):
    response = None
    if request.is_ajax():
        try:
            response = getSpeciesVersionList()
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'rightpanel.html', context)


def downloadFile(request):
    response = None
    try:
        filepath = request.GET.get('path')
        req_url = 'https://de.cyverse.org/terrain/secured/fileio/download?path=' + filepath
        r = requests.get(req_url, headers={'Authorization': 'BEARER '+settings.ACCESS_TOKEN})
        return HttpResponse(r)
    except:
        e = sys.exc_info()
        return HttpResponse(e)

@csrf_exempt
def Signout(request):
    response = None
    try:
        data_attrib = json.loads(request.body)
        if 'isRedirect_SavedPage' in data_attrib:
            isRedirect_SavedPage = bool(data_attrib['isRedirect_SavedPage'])

        sessionid = getSessionIdfromCookie(request.COOKIES)
        redis_sessiondata = getSessionData(sessionid)
        user_name = getUserNamefromSessionData(redis_sessiondata)
        savedPath = cache.get(user_name)
        if (savedPath and (not isRedirect_SavedPage)):
            cache.delete(user_name)
        cache.delete(sessionid)
        response = HttpResponse(settings.ROOT_URL, content_type='text/plain')
        response.delete_cookie(key='sessionId')
        return response

    except:
        e = sys.exc_info()
        return HttpResponse(e)

@csrf_exempt
def getAnalyze(request):
    data_attrib = json.loads(request.body)
    if 'redirect_savedurl' in data_attrib:
        redirect_savedurl = str(data_attrib['redirect_savedurl'])
    else:
        redirect_savedurl = ''

    sessionid = getSessionIdfromCookie(request.COOKIES)
    redis_sessiondata = getSessionData(sessionid)
    user_name = getUserNamefromSessionData(redis_sessiondata)
    limit = str(data_attrib['limit'])
    offset = str(data_attrib['offset'])
    try:
        response = getAnalysisHistory(limit, offset,"enddate","ASC",redis_sessiondata)
    except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
    except PermissionError as e:
        response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 401
        cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
        return response
    except:
    	e = sys.exc_info()
    	return HttpResponse(e)
    return HttpResponse(response, content_type='application/json')

def SyncAppDataToBioViz(request):
    accessToken = request.GET.get('accesstoken')
    try:
       response_json = getLatestApps(accessToken)
    except:
        e = sys.exc_info()
        return HttpResponse(e)
    return HttpResponse(response_json, content_type='application/json')

def getHtmlDataByAppId(request):
    AppId = request.GET.get('AppId')
    try:
       response_json = getHtmlDataForAppId(AppId)
    except:
        e = sys.exc_info()
        return HttpResponse(e)
    return HttpResponse(response_json)

@csrf_exempt
def runCoverageGraphAnalysis(request):
    try:
       sessionid = getSessionIdfromCookie(request.COOKIES)
       redis_sessiondata = getSessionData(sessionid)
       user_name = getUserNamefromSessionData(redis_sessiondata)
       accesstoken = getaccesstokenfromredis(redis_sessiondata)
       data_attrib = json.loads(request.body)
       AppId = data_attrib['AppId']
       output_dir = data_attrib['outputdir']
       section = str(data_attrib['currentsection'])
       Analysis_name = str(data_attrib['Analysisname'])
       config = data_attrib['config']
       if 'redirect_savedurl' in data_attrib:
           redirect_savedurl = str(data_attrib['redirect_savedurl'])
       else:
           redirect_savedurl = ''

       if not Analysis_name:
           Appname = str(getNameForApp(AppId))
       else:
           Appname = Analysis_name

       if (section != 'home'):
           username = getUserNamefromSessionData(redis_sessiondata)
           home_path_prefix = getHomePath_Prefx(username, redis_sessiondata)
           ## set output directory to home/analyses if the current directory is not home to avoid failure.
           output_dir = home_path_prefix+username+'/'+'analyses'

       response_json = runRestCoverageGraphAnalysis(Appname, AppId, 'de', False, False, False, output_dir, True, config, accesstoken)
    except LookupError as e:
        response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 400
        return response
    except PermissionError as e:
        response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 401
        cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
        return response
    except FileNotFoundError as e:
        response = HttpResponse(json.dumps({'message': 'Path does not exist'}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, redirect_savedurl, timeout=settings.REDIS_EXPIRY)
        return response
    except:
        e = sys.exc_info()
        return HttpResponse(e)
    return HttpResponse(response_json)

def getAppsByFileFormat(request):
    format = request.GET.get('Format')
    try:
        return HttpResponse(getCompatibleAppsByformat(format))
    except:
        e = sys.exc_info()
        return HttpResponse(e)
    return HttpResponse(response_json)

@csrf_exempt
def createFolder(request):
    data_attrib = json.loads(request.body)
    if 'path' in data_attrib:
        path = str(data_attrib['path'])
    else:
        raise FileExistsError()

    sessionid = getSessionIdfromCookie(request.COOKIES)
    redis_sessiondata = getSessionData(sessionid)
    user_name = getUserNamefromSessionData(redis_sessiondata)
    try:
        response = createDirectory(path, redis_sessiondata)
    except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
    except PermissionError as e:
        response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 401
        cache.set(user_name, path, timeout=settings.REDIS_EXPIRY)
        return response
    except FileExistsError as e:
        response = HttpResponse(json.dumps({'message': 'File Exists! '}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, path, timeout=settings.REDIS_EXPIRY)
        return response
    except:
    	e = sys.exc_info()
    	return HttpResponse(e)
    return HttpResponse(response, content_type='application/json')


@csrf_exempt
def getAccessTokenForDownload(request):
    res = None
    try:
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)

            response = ResponseParser.parse_AccessToken(accesstoken)
            custom_encoder = CustomEncoder()
            res = custom_encoder.encode(response)
    except LookupError as e:
            response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
            response.status_code = 400
            return response
    except:
            e = sys.exc_info()
            return HttpResponse(e)
    return HttpResponse(res, content_type='application/json')

@csrf_exempt
def uploadFile(request):
    sessionid = getSessionIdfromCookie(request.COOKIES)
    redis_sessiondata = getSessionData(sessionid)
    user_name = getUserNamefromSessionData(redis_sessiondata)
    dest = request.POST['dest']
    try:
        files = request.FILES
        response = upload_File(files['file'].file, str(files['file']), dest, redis_sessiondata)
    except FileExistsError as e:
        response = HttpResponse(json.dumps({'message': 'FileExists'}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, dest, timeout=settings.REDIS_EXPIRY)
        return response
    except LookupError as e:
        response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
        response.status_code = 400
        return response
    except PermissionError as e:
        response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 401
        cache.set(user_name, dest, timeout=settings.REDIS_EXPIRY)
        return response
    except:
    	e = sys.exc_info()
    	return HttpResponse(e)
    return HttpResponse(response, content_type='application/json')

@csrf_exempt
def renameFileFolder(request):
    data_attrib = json.loads(request.body)
    if 'source' in data_attrib:
          source = str(data_attrib['source'])
    if 'dest' in data_attrib:
          dest = str(data_attrib['dest'])

    sessionid = getSessionIdfromCookie(request.COOKIES)
    redis_sessiondata = getSessionData(sessionid)
    user_name = getUserNamefromSessionData(redis_sessiondata)
    try:
        response = rename_FileFolder(source, dest, redis_sessiondata)
    except FileExistsError as e:
        response = HttpResponse(json.dumps({'message': 'FileExists'}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, source, timeout=settings.REDIS_EXPIRY)
        return response
    except LookupError as e:
        response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
        response.status_code = 400
        return response
    except PermissionError as e:
        response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 401
        #cache.set(user_name, source, timeout=settings.REDIS_EXPIRY)
        return response
    except:
    	e = sys.exc_info()
    	return HttpResponse(e)
    return HttpResponse(response, content_type='application/json')

@csrf_exempt
def uploadFileByURL(request):
    data_attrib = json.loads(request.body)
    if 'address' in data_attrib:
          address = str(data_attrib['address'])
    if 'dest' in data_attrib:
          dest = str(data_attrib['dest'])
    sessionid = getSessionIdfromCookie(request.COOKIES)
    redis_sessiondata = getSessionData(sessionid)
    user_name = getUserNamefromSessionData(redis_sessiondata)
    try:
        response = uploadFileBy_Url(address, dest, redis_sessiondata)
        print("Response: ",response)
    except FileExistsError as e:
        response = HttpResponse(json.dumps({'message': 'FileExists'}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, address, timeout=settings.REDIS_EXPIRY)
        return response
    except LookupError as e:
        response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
        response.status_code = 400
        return response
    except PermissionError as e:
        response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 401
        #cache.set(user_name, source, timeout=settings.REDIS_EXPIRY)
        return response
    except:
    	e = sys.exc_info()
    	return HttpResponse(e)
    return HttpResponse(response, content_type='application/json')
@csrf_exempt
def moveFileFolder(request):
    data_attrib = json.loads(request.body)
    if 'source' in data_attrib:
          source = data_attrib['source']
    if 'dest' in data_attrib:
          dest = str(data_attrib['dest'])
    sessionid = getSessionIdfromCookie(request.COOKIES)
    redis_sessiondata = getSessionData(sessionid)
    user_name = getUserNamefromSessionData(redis_sessiondata)
    try:
        response = move_FileFolder(source, dest, redis_sessiondata)
    except FileExistsError as e:
        response = HttpResponse(json.dumps({'message': 'FileExists'}),content_type='application/json')
        response.status_code = 403
        cache.set(user_name, source, timeout=settings.REDIS_EXPIRY)
        return response
    except FileNotFoundError as e:
        response = HttpResponse(json.dumps({'message': 'Destination Does not exist'}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, source, timeout=settings.REDIS_EXPIRY)
        return response
    except BufferError as e:
        response = HttpResponse(json.dumps({'message': 'Too many sources to process'}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, source, timeout=settings.REDIS_EXPIRY)
        return response
    except LookupError as e:
        response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
        response.status_code = 400
        return response
    except PermissionError as e:
        response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 401
        #cache.set(user_name, source, timeout=settings.REDIS_EXPIRY)
        return response
    except:
    	e = sys.exc_info()
    	return HttpResponse(e)
    return HttpResponse(response, content_type='application/json')

@csrf_exempt
def deleteFileFolder(request):
    data_attrib = json.loads(request.body)
    if 'paths' in data_attrib:
          paths = data_attrib['paths']

    sessionid = getSessionIdfromCookie(request.COOKIES)
    redis_sessiondata = getSessionData(sessionid)
    user_name = getUserNamefromSessionData(redis_sessiondata)
    try:
        response = delete_FileFolder(paths,redis_sessiondata)
    except FileExistsError as e:
        response = HttpResponse(json.dumps({'message': 'FileExists'}),content_type='application/json')
        response.status_code = 403
        cache.set(user_name, list, timeout=settings.REDIS_EXPIRY)
        return response
    except FileNotFoundError as e:
        response = HttpResponse(json.dumps({'message': 'Destination Does not exist'}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, list, timeout=settings.REDIS_EXPIRY)
        return response
    except BufferError as e:
        response = HttpResponse(json.dumps({'message': 'Too many sources to process'}),
                                content_type='application/json')
        response.status_code = 403
        cache.set(user_name, list, timeout=settings.REDIS_EXPIRY)
        return response
    except LookupError as e:
        response = HttpResponse(json.dumps({'message': 'Local Auth token Expired'}),
                                    content_type='application/json')
        response.status_code = 400
        return response
    except PermissionError as e:
        response = HttpResponse(json.dumps({'message': 'Auth token Expired'}),
                                content_type='application/json')
        response.status_code = 401
        #cache.set(user_name, source, timeout=settings.REDIS_EXPIRY)
        return response
    except:
    	e = sys.exc_info()
    	return HttpResponse(e)
    return HttpResponse(response, content_type='application/json')

def initializeUserWorkspace(request):
    try:
        sessionid = getSessionIdfromCookie(request.COOKIES)
        redis_sessiondata = getSessionData(sessionid)
        accesstoken = getaccesstokenfromredis(redis_sessiondata)
        response_json = runInitializeUserWorkspace(accesstoken)
    except:
        e = sys.exc_info()
        return HttpResponse(e)
    return HttpResponse(response_json)
