# BioViz *Connect*

BioViz *Connect* is a web application linking CyVerse science gateway resources to genomic visualization in the [Integrated Genome Browser](https://bioviz.org).

To see it in action, visit the [BioViz Web site](https://bioviz.org) and select BioViz *Connect*.

Read about how BioViz *Connect* works in [this preprint on bioRxiv](https://www.biorxiv.org/content/10.1101/2020.05.15.098533v3.full).

* * *

## FAQ

### Why did you create BioViz *Connect*?

We created BioViz *Connect* so that researchers could easily visualize genomic data stored in their CyVerse Discovery Environment accounts using [Integrated Genome Browser](https://bioviz.org). 

### What can I do with BioViz *Connect*?

You can:

* Use [IGB](https://bioviz.org) to visualize data files stored in your CyVerse account
* Control how data files will look once loaded into [IGB](https://bioviz.org) by annotating files with meta-data
* Create public links to your data files, which you can then share with others
* Upload files to your CyVerse account from your computer or URL
* Delete and move files and folders

### How do I log in to BioViz *Connect*?

Visit the [BioViz.org Web site](https://bioviz.org) and select BioViz *Connect* to log in. Follow the links to log in using your CyVerse account credentials.

### How do I get a CyVerse Discovery Environment account? 

Create a free CyVerse Discovery Environment account on the [Cyverse.org Web site](https://cyverse.org).

### What is CyVerse?

CyVerse is funded by the National Science Foundation with a mission to design, deploy, and expand a national cyberinfrastructure for life sciences research and train scientists in its use.

CyVerse provides many diverse resources for scientists, including the ability to store, share, publish, and analyze data files. 

To find out more about CyVerse, visit the [Cyverse.org Web site](https://cyverse.org).

### What is the Discovery Environment?

The Discovery Environment provides a web interface for cloud-based computing and data storage.

### Who developed BioViz *Connect*?

BioViz *Connect* functionality and interface was designed by Dr. Nowlan Freese with Karthik Raveendran. Karthik Raveendran implemented the user interface. Chaitanya Kintali, Srishti Tiwari, and Pawan Bole implemented server-side connectivity with the Terrain API. Chester Dias and Ann Loraine designed and wrote the BioViz *Connect* deployment system.

The BioViz team would also like to thank Paul Sarando, Sarah Roberts, Sriram Srinivasan, Ian McEwen, Ramona Walls, and Reetu Tuteja for their assistance with the Terrain API and publishing CyVerse apps, which was made possible through CyVerse's External Collaborative Partnership program.

### How does it work?

BioViz *Connect* accesses CyVerse storage and compute resources by calling Terrain API endpoints. BioViz *Connect* forwards the address of those resources (as publicly accessible URLs) to Integrated Genome Browser by hitting a REST API endpoint within Integrated Genome Browser.

BioViz *Connect* is a JavaScript-based user interface with a python3 Django back-end that manages authentication and communication with Terrain API endpoints. 

### Can I deploy my own copy of BioViz Connect?

Yes, you can do that. 

We recommend using the [BioViz Connect ansible playbooks](https://bitbucket.org/lorainelab/bioviz-connect-playbooks) to deploy your own instance of BioViz *Connect*. 

Before your instance can access the CyVerse Terrain endpoints, it needs to be configured with a client id and a client secret credential,
which you can get from CyVerse. Feel free to contact us (see below) for help with that. 

### I have questions. Who can help? 

If you have questions, contact:

* [Karthik Raveendran](kraveend@uncc.edu)
* [Nowlan Freese](nfreese@uncc.edu)
* [Ann Loraine](aloraine@uncc.edu)
* [Dr. Ann Loraine](aloraine@uncc.edu)
