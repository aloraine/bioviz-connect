//rightPanel.js
var specVer;
function rightPanel(section) {
	$(".right").data("currentrightpanel",section)
	$(".right").find('.pathLink').html("<h6><i class='text-dark p-1'>"+ $(".right").data("fileInfo").label+"</i></h6>")
	user_permission = $(".right").data("fileInfo").permission
	isfolder = $("#pop_metadata").data("isfolder")
    $(".right").css("display", "block");
    rpStatus = 1
    $(".right").children().css("display", "none");
    $(section).css("display", "block");
    loadingDisplay(".rightLoad",true)
    if(section=="#makePublic"){
    	if (user_permission=='own'){
    		        checkFilePermissionInitial($(".right").data("fileInfo").path)
		}
    	else{
					checkFilePermission_Search($(".right").data("fileInfo").path)
		}
    }else if(section=="#metadata"){
        checkMetaDataInitial($(".right").data("fileInfo").path, isfolder)
    }else if(section=="#analysis"){
    	path = $(".right").data("fileInfo").path
		directories = path.split('/')
		filename = directories[directories.length-1]
		split_filename = filename.split('.')
		format = split_filename[split_filename.length-1]
	    analyseUI($(".right").data("fileInfo").path, format);
    }
}
function closerp() {
    $(".right").css("display","none").animate();
    rpStatus = 0;
}


function checkFilePermission_Search(path){
	object = {}
	object['path'] = path
	object['redirect_savedurl'] = location.hash.split('#')[1]
    object['current_section_search'] = main_Section

    urlLink=location.origin+"/checkFilePermission_Search/";
	var json = JSON.stringify(object)

	$.ajax({
		method: "POST",
        data: json,
        processData: false,
        contentType: false,
        dataType: "json",
		url: urlLink,
	    success: function (data, status, jqXHR) {
        	var userPerm=data.paths[0]["user-permissions"]
            for(var i=0;i<userPerm.length;i++){
                if(userPerm.length!=0 && (userPerm[i].user=="anonymous" || userPerm[i].user=="anonymous#iplant") && userPerm[i].permission=="read"){
			        manageLinkUI("create","https://data.cyverse.org/dav-anon"+path)
                   	break;
			    }else{
                	manageLinkUI("remove",path)
			}
		}
		loadingDisplay(".rightLoad",false)
	},
	error: function (jqXHR, status, err) {
			if(jqXHR.status == 401) {
				signout(true)
			}else{
        			alert("Sorry! Could not check File permissions for this file. Please try again or contact admin.")
			}
	},
        complete: function (jqXHR, status) {
        }
    });
}

// Manage Link
function checkFilePermissionInitial(path){
	object = {}
	object['path'] = path
	object['redirect_savedurl'] = location.hash.split('#')[1]

    urlLink=location.origin+"/checkFilePermission/"
	var json = JSON.stringify(object)

    $.ajax({
		method: "POST",
        data: json,
        processData: false,
        contentType: false,
        dataType: "json",
       	async: "true",
		url: urlLink,
	success: function (data, status, jqXHR) {
        	var userPerm=data.paths[0]["user-permissions"]
            for(var i=0;i<userPerm.length;i++){
        	    if(userPerm.length!=0 && userPerm[i].user=="anonymous" && userPerm[i].permission=="read"){
				manageLinkUI("create","https://data.cyverse.org/dav-anon"+path)
                break;
			}else{
                manageLinkUI("remove",path)
			 }
		}
		loadingDisplay(".rightLoad",false)
	},
	error: function (jqXHR, status, err) {
		if(jqXHR.status == 401){
                signout(true)
		}else{
        			alert("Sorry! Could not check File permissions for this file. Please try again or contact admin.")
		}
	},
        complete: function (jqXHR, status) {
        }
    });
}
function encodePath(path){

	var words = path.split('/')
    index = 0
    wordslength = words.length

    words.forEach(function (e) {
                    if (e=='https:')
                    {
                        index = index + 1
                        return;
                    }
                    encodedfilename = encodeURIComponent(e)
                    words[index] = encodedfilename
                    index = index + 1
               });
    encodedfilepath = words.join('/')
    return encodedfilepath
}
function manageLinkUI(ele,path) {
    	if (ele == "remove") {
        	$('#remove').css("display", "none")
        	$('#create').css("display", "block")
        	$('.remove').addClass("d-none")
        	$('.create').removeClass("d-none")
        	$('#plink').html("Not Available")
        	$('#copy').css("visibility", 'hidden')
        	$('.rounded-circle').removeClass("btn-success")
        	$('.rounded-circle').addClass("btn-warning")
        	$('.fileStatus').html("private")
    	} else {
        	$('#remove').css("display", "block")
        	$('#create').css("display", "none")
        	$('.remove').removeClass("d-none")
        	$('.create').addClass("d-none")
			encodedpath = encodePath(path)
        	$('#plink').html("<a target='_blank' href='"+encodedpath+"'>"+encodedpath+"</a>")
        	$('#copy').css("visibility", 'visible')
        	$('.rounded-circle').addClass("btn-success")
        	$('.rounded-circle').removeClass("btn-warning")
        	$('.fileStatus').html("public")
	}
}

function manageLinkOperations(ele, openigb){
	loadingDisplay(".rightLoad",true)
    loadingDisplay(".middleLoad",true)

	var object = {}
	object['path'] = $(".right").data("fileInfo").path
	object['is_anonymous'] = true

	var urlLink=""
	if (ele == "remove") {
		urlLink=location.origin+"/remPublicLink/"
	}else{
		object['is_anonymous'] = true
		urlLink=location.origin+"/setPublicLink/"
	}
	object['redirect_savedurl'] = location.hash.split('#')[1]
	var json = JSON.stringify(object)

	$.ajax({
        data: json,
        processData: false,
        contentType: false,
        type: 'POST', async: "true",
		url: urlLink,
		success: function (data, status, jqXHR) {
	            var viewinigb_id  = 'view_'+$(".right").data("fileInfo").id
                var button_element = document.getElementById(viewinigb_id)
        		if(ele=="create"){
				if(data.sharing[0].user=="anonymous" && data.sharing[0].paths[0].permission=="read"){
					manageLinkUI(ele,data.sharing[0].paths[0].path)
				}
				loadingDisplay(".rightLoad",false)
                loadingDisplay(".middleLoad",false)

				$('.publicCreate').toast('show');
				button_element.classList.remove('btn-warning')
                button_element.classList.add('btn-success')
                var onclick = button_element.getAttribute('onclick')
                var modified_onclick = onclick.replace('btn-warning', 'btn-success')
                button_element.setAttribute('onclick', modified_onclick)
			}else{
				if(data.unshare[0].user=="anonymous" && data.unshare[0].unshare[0].success==true){
					manageLinkUI(ele,data.unshare[0].unshare[0].path)
				}
				loadingDisplay(".rightLoad",false)
                loadingDisplay(".middleLoad",false)
				$('.publicRemoved').toast('show');
				button_element.classList.remove('btn-success')
                button_element.classList.add('btn-warning')
                var onclick = button_element.getAttribute('onclick')
                var modified_onclick = onclick.replace('btn-success', 'btn-warning')
                button_element.setAttribute('onclick', modified_onclick)
			}

			if (openigb){
					path = $(".right").data("fileInfo").path
					id = $(".right").data("fileInfo").id
				    viewInIgb(path, id, 'btn-success','')

			}
		},
		error: function (jqXHR, status, err) {
				if(jqXHR.status == 401){
                		signout(true)
				}else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }else{
					alert(err)
				}},
        	complete: function (jqXHR, status) {
		}
	});
}

// MetaData
function checkMetaDataInitial(path, isfolder){
	loadingDisplay(".rightLoad",true)
	files_supported = 'cnchp,lohchp,bar,gff3,gtf,bgn,bgr,bp1,bps,brpt,brs,bsnp,chp,cnt,cyt,ead,fa,fas,fasta,fna,fsa,mpfa,fsh,gb,gen,gr,link.psl,bnib,sin,egr,egr.txt,map,cn_segments,loh_segments,sgr,2bit,useq,var,wig,bdg,bedgraph,vcf,sam,bed,gff,psl,psl3,pslx,bam,axml,bigbed,bb,bw,bigWig,bigwig,das,dasxml,das2xml,narrowPeak,narrowpeak,broadPeak,broadpeak,tally,gz'
    files_supported_list = files_supported.split(',')
    path_extension = path.split(".")[path.split(".").length-1]
//Get Species and Version List
	if (isfolder || jQuery.inArray(path_extension, files_supported_list) == -1){
	    $("#Filepath_metadata").addClass("d-none")
		$("#genomeversion").addClass("d-none")
		$("#trackcolor").addClass("d-none")
		$("#metaTrackName").parent().addClass("d-none")
	}
	else{
		$("#Filepath_metadata").removeClass("d-none")
		$("#genomeversion").removeClass("d-none")
		$("#trackcolor").removeClass("d-none")
		$("#metaTrackName").parent().removeClass("d-none")
	}
   	$(".species").children("button").html("Species...")
   	$(".version").children("button").html("Version...")
   	$('.fgcp').val("#0000FF")
   	$('.bgcp').val("#FFFFFF")
	$("#comments").val('')
	$("#metaTrackName").val($(".right").data("fileInfo").label);
   	$.ajax({
       	method: "GET",
       	dataType: "json",
       	async: "true",
	    url: location.origin+"/getSpeciesVersionList/",
	    success: function (data, status, jqXHR){
            specVer=data
       	},
       	error: function (jqXHR, status, err) {
            alert("Sorry! Unable to check metadata for this file.Please try again or contact admin.")
		},
           	complete: function (jqXHR, status) {
        }
   	});
//check the Metadata data for the file
   	retrieveMetaData($(".right").data("fileInfo").id)
}
function retrieveMetaData(fileId){
	    var object = {};
        object['fileId'] = fileId
        object['redirect_savedurl'] = location.hash.split('#')[1]
        if (object['redirect_savedurl'].includes('searchDirectory')){
            object['current_section_search'] = main_Section
        }

        var json = JSON.stringify(object);

        var metaData=""
        $.ajax({
        method: "POST",
        dataType: "json", data: json,
        async: "true",
		url: location.origin+"/retrieveMetaData/",
	    success: function (data, status, jqXHR) {
		        metaDataUI(data)
            	loadingDisplay(".rightLoad",false)
        },

        error: function (jqXHR, status, err) {
        	if(jqXHR.status == 401) {
        		signout(true)
        	}else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }else{
        		alert("Sorry! Unable to retrieve metadata for this file. Please try again or contact admin.")
			}
	    },
            complete: function (jqXHR, status) {
        }
	});
}
function metaDataUI(metaData){
	for(i=0;i<metaData["avus"].length;i++){
		if(metaData["avus"][i]["attr"]=="Genome"){
            		var genome=metaData["avus"][i]["value"].split(";")
            		$(".species").children("button").html(genome[0])
            		$(".version").children("button").html(genome[1])
        	}else if(metaData["avus"][i]["attr"]=="foreground"){
            		$('.fgcp').val(metaData["avus"][i]["value"])
        	}else if(metaData["avus"][i]["attr"]=="background"){
            		$('.bgcp').val(metaData["avus"][i]["value"])
        	}else if(metaData["avus"][i]["attr"]=="Comments"){
					$('#comments').val(metaData["avus"][i]["value"])
			}else if(metaData["avus"][i]["attr"]=="trackName"){
					$('#metaTrackName').val(metaData["avus"][i]["value"])
			}
    	}

    	$(".species").children("div").html("")
    	var specLen=Object.keys(specVer).length;
    	for(i=0;i<specLen;i++){
        	$(".species").children("div").append("<a class='dropdown-item' onclick='dropDownPopulate("+i+")'>"+Object.keys(specVer)[i]+"</a>")
    	}
}
function dropDownPopulate(n,data){
	$(".species").children("button").html(Object.keys(specVer)[n])
    	$(".version").children("button").html("Version...")
    	$(".version").children("div").html("")
    	var verData=specVer[Object.keys(specVer)[n]]
    	var verlen=verData.length;
    	for(i=0;i<verlen;i++){
        	$(".version").children("div").append("<a class='dropdown-item' onclick='$(\".version\").children(\"button\").html(\""+verData[i]+"\")'>"+verData[i]+"</a>")
    	}
}
function saveMetaData(){
    loadingDisplay(".rightLoad",true)
    var fd = new FormData();
    var genome=$(".species").children("button").html()+";"+$(".version").children("button").html()
    var trackname = $("#metaTrackName").val();
    fd.append( 'fileId', $(".right").data("fileInfo").id );
    fd.append('data',
    '[{"attr":"Genome","unit":"integratedGenomeBrowser","value":"'+genome+'"},\n'+
    '{"attr":"trackName","unit":"integratedGenomeBrowser","value":"'+trackname+'"},\n'+
    '{"attr":"foreground","unit":"integratedGenomeBrowser","value":"'+$('.fgcp').val()+'"},\n'+
    '{"attr":"Comments","unit":"integratedGenomeBrowser","value":"'+encodeURIComponent($('#comments').val())+'"},\n'+
    '{"attr":"background","unit":"integratedGenomeBrowser","value":"'+$('.bgcp').val()+'"}]')

    $.ajax({
        url: location.origin + '/saveMetaData/',
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data){
            loadingDisplay(".rightLoad",false)
	        $('.metaSave').toast('show');
        },
        error: function (jqXHR, status, err) {
            alert(err)
	    },
            complete: function (jqXHR, status) {
        }
    });
}

function copyLink(){
	$("#plink").attr({disabled:false});
	$("#plink").select();
	document.execCommand("copy");
	$("#plink").attr({disabled:true});
	$(".copiedToast").toast("show");

}


