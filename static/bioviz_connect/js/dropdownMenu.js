function createFolderModal(){
   $('#newFolder span').html("");
   $('#newFolder').data("fileInfo",location.hash.replace('#',""))
   $('#newFolder').modal('show');
   $('#newFolder .newFolderTitle').val("Untitled Folder");
}
function createNewFolder(element){
    $('#newFolder span').html("");
    element.innerHTML=" <i class='fas fa-sync fa-spin'></i>"
    var originalPath = $("#newFolder").data("fileInfo");
    var jsonPath = {}
    jsonPath['path']= originalPath+"/"+encodeURIComponent($('#newFolder .newFolderTitle').val())
    $.ajax({
		method: "POST",
        dataType: "html",
        async: "true",
        data: JSON.stringify(jsonPath),
		url: location.origin+"/createFolder/",
	    success: function (data, status, jqXHR) {
	        element.innerHTML="Create";
            location.reload();
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
	        $('#newFolder span').html("<strong class='mr-auto text-danger'>"+JSON.parse(jqXHR.responseText)['message']+"</strong>");
	        element.innerHTML="Create";
	    }
    });
}
function renameFileFolderModal(){
    $('#renameFileFolder').data("fileInfo",$("#context-menu").data("fileInfo"))
    $('#renameFileFolder .renameText').val($('#renameFileFolder').data("fileInfo").label);
    $('#renameFileFolder').modal('show');
    if($("#context-menu").data("fileType") == "folder"){
         $('#renameFileFolder .renameFolderWarning').removeClass("d-none");
         $('#renameFileFolder .renameFolderWarning').addClass("d-block");
    }else{
        $('#renameFileFolder .renameFolderWarning').removeClass("d-block");
        $('#renameFileFolder .renameFolderWarning').addClass("d-none");
    }
}
function renameFileFolder(element){
    element.innerHTML=" <i class='fas fa-sync fa-spin'></i>"
    var originalPath = $("#context-menu").data("fileInfo").path
    var parentPath =originalPath.substring(0,originalPath.lastIndexOf("/"));
    var json = {}
    json['source']= uriEncodeFileName($("#context-menu").data("fileInfo").path);
    json['dest']= uriEncodeFileName(parentPath+"/"+$('#renameFileFolder .renameText').val())
    $.ajax({
		method: "POST",
        dataType: "html",
        async: "true",
        data: JSON.stringify(json),
		url: location.origin+"/renameFileFolder/",
	    success: function (data, status, jqXHR) {
            location.reload();
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
            $('#renameFileFolder .renameError').html("<strong class='mr-auto text-danger'>"+JSON.parse(jqXHR.responseText)['message']+"</strong>");
            element.innerHTML="Save Changes";
	    }
    });
}
function copy_Path(){
    var cpyPth=$("#context-menu").data("fileInfo").label;
    var tempInput = document.createElement('INPUT');
    $("body").append(tempInput);
    tempInput.setAttribute('value',cpyPth)
    tempInput.select();
    document.execCommand('copy');
    $(tempInput).remove();
}
function uploadByURLUI(){
   $('#uploadURL span').html("");
   $('#uploadURL').data("fileInfo",location.hash.replace('#',""))
   $('#uploadURL').modal('show');
}
function uploadFileFolder(){
   $('#uploadFileFolder span').html("");
   $('#uploadFileFolder').data("fileInfo",location.hash.replace('#',""))
   $('#uploadFileFolder').modal('show');
}
function uploadByURL(element){
	element.innerHTML=" <i class='fas fa-sync fa-spin'></i>"
    var json = {}
    var address = $(".uploadURLTitle").val();
    var dest = location.hash.replace('#',"");
    json['address']= address;
    json['dest']= dest
    if(address == ""){
		$('.uploadURLError').html("<strong class='mr-auto text-danger'>Please Enter a URL</strong>")
		element.innerHTML="Upload"
    }else{
     $.ajax({
		method: "POST",
        dataType: "html",
        async: "true",
        data: JSON.stringify(json),
		url: location.origin+"/uploadFileByURL/",
	    success: function (data, status, jqXHR) {
	        $('.uploadURLError').html("")
		    if(data == "error_codereason"){
		        $('.uploadURLError').html("<strong class='mr-auto text-danger'>URL Error: Please check the URL</strong>")
		        element.innerHTML="Upload"
		    }else{
		        $('.uploadURLError').html("<i class='fas text-success fa-check mr-2 fa-lg'></i><strong class='mr-auto text-info'>Upload Scheduled!</strong>")
		        setTimeout(function(){
		            element.innerHTML="Upload"
		            $('#uploadURL').modal('hide');
		            $('.uploadFileByUrl').toast('show');
		            },
		        5000);
		    }
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
            $('.uploadURLError').html("<strong class='mr-auto text-danger'>"+JSON.parse(jqXHR.responseText)['message']+"</strong>")
            element.innerHTML="Upload";
	    }
    });
      }
}
function upload(element){
        var data = new FormData();
        // Get form
        var form = $('#uploadFileFolderInput')[0].files[0];
        var dest = location.hash.split('#')[1] // get the current location path
        data.append('file', form)
        data.append('dest', dest)
		// disabled the submit button
		element.innerHTML=" <i class='fas fa-sync fa-spin'></i>"
		$('.uploadFileToast').toast('show');
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: location.origin+"/uploadFile/",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                $("#result").text(data);
				$('#uploadFileFolder').modal('hide');
                setTimeout(location.reload(),5000);
            },
            error: function (e) {
                element.innerHTML= "Upload"
                if (e.status == 200){
                    $("#result").text(data);
                    $('#uploadFileFolder').modal('hide');
                    setTimeout(location.reload(),5000);
                }
                else{
                    $("#result").text(e.responseText);
                    console.log("ERROR : ", e);
                    $(".uploadFileFolderError").html("ERROR : "+ e.statusText)
                 }
            }
        });
}
function downloadFile(){
	var accesstoken = ""
	var path = $("#context-menu").data("fileInfo").path;
	var filename = $("#context-menu").data("fileInfo").label;
	$.ajax({
		method: "GET",
        dataType: "json",
        async: "true",
		url: location.origin+"/getAccessTokenForDownload/",
	    success: function (data, status, jqXHR) {
            $(".downloadToast").toast("show");
            accesstoken = data["accesstoken"]
            downloadFile_client(path,accesstoken,filename);
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
	    }
    });
}
function downloadFile_client(path, accesstoken, filename){
	xhttp = new XMLHttpRequest();
	url = 'https://de.cyverse.org/terrain/secured/fileio/download?path='+path
	bearer_header = "Bearer "+accesstoken
	xhttp.open("GET", url);
	xhttp.setRequestHeader("Authorization", bearer_header);
	// You should set responseType as blob for binary responses
	xhttp.responseType = 'blob';
	xhttp.send();
	xhttp.onload = function(e) {
        if (this.status == 200) {
            var blob = new Blob([this.response], {type:"application/octet-stream"});
            let a = document.createElement("a");
            a.style = "display: none";
            document.body.appendChild(a);
            let url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = filename;
            a.click();
            window.URL.revokeObjectURL(url);
        }else{
        //deal with your error state here
        }
     };
}
function selectedRow(row_id, condition){
	if(condition){
		$('#' + row_id).addClass('cut_row');
	}else{
		$('#' + row_id).removeClass('cut_row');
	}
}
function cutFileFolder(){
	$('#pop_paste').removeClass("disabled");
	$('#pop_paste').removeClass("text-secondary");
	var row_id = $("#context-menu").data("fileInfo").id
	var filepath = $("#context-menu").data("fileInfo").path;
	var storage = window.sessionStorage;
	var cut_path_list = JSON.parse(storage.getItem("cut_path_list"))
	var cut_id_list = JSON.parse(storage.getItem("cut_id_list"))
	selectedRow(row_id,true);
	if(typeof cut_path_list == 'undefined' || cut_path_list == null){
		cut_path_list = [filepath];
		cut_id_list = [row_id]
	}else{
		cut_path_list.push(filepath);
		cut_id_list.push(row_id);
	}
	storage.setItem("cut_path_list",JSON.stringify(cut_path_list));
	storage.setItem("cut_id_list",JSON.stringify(cut_id_list))
}
function pasteFileFolder(check){
	var json = {}
	var storage = window.sessionStorage;
	if(check){
	    var dest = $("#context-menu").data("fileInfo").path
	}else{
	    var dest = decodeURIComponent($("#popup-bc-paste").data("path"))
	}
	var cut_path_list = JSON.parse(storage.getItem("cut_path_list"))
	var cut_id_list = JSON.parse(storage.getItem("cut_id_list"))
    json['source']= cut_path_list;
    json['dest']= dest
    storage.setItem("cut_id_list",null);
	storage.setItem("cut_path_list",null);
   $.ajax({
		method: "POST",
        dataType: "html",
        async: "true",
        data: JSON.stringify(json),
		url: location.origin+"/moveFileFolder/",
	    success: function (data, status, jqXHR) {
	        $("#pop_paste").addClass("disabled");
			$('#pop_paste').addClass("text-secondary");
			$(".pasteToast").toast('show')
			setTimeout(function(){
				location.reload();
				$(".cut_row").removeClass('cut_row');
			 }, 10000);
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
	    }
    });
}
var deleteJSON = {}
function deleteFileFolder(){
	var row_id = $("#context-menu").data("fileInfo").id
	deleteJSON["paths"] = [$("#context-menu").data("fileInfo").path];
	selectedRow(row_id,true);
	$("#deleteFileFolderModal").data("row_id",row_id);
	$('#deleteFileFolderModal').modal('show');
}

function deleteFileFolderYes(element){
	element.innerHTML=" <i class='fas fa-sync fa-spin'></i>"
	$.ajax({
		method: "POST",
        dataType: "json",
        async: "true",
        data: JSON.stringify(deleteJSON),
		url: location.origin+"/deleteFileFolder/",
	    success: function (data, status, jqXHR) {
	    },
	    error:function(jqXHR, status, err){
	        if (jqXHR.status == 401){
                    signout(true)
            }
            else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }
            if(jqXHR.status == 200){
                deleteJSON = {};
				$("#deleteFileFolderModal .deleteFileFolderError strong").html("<i class='fa fa-check mr-2 fa-lg'></i>Delete Scheduled!")
				setTimeout(function(){
					element.innerHTML="Yes"
					$('#deleteFileFolderModal').modal('hide');
					location.reload();
			    }, 10000);


            }
	    }
    });
}
