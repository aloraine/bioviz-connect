$(document).ready(function (e) {
    getAuth()
});

function getAuth(){
    $.ajax({
        type: "GET",
        dataType: "html",
        async: "true",
        url: location.origin+"/login",
            success: function (data, status, jqXHR) {
                // data.redirect contains the string URL to redirect to
                window.location.replace(data);
            },
             error: function (jqXHR, status, err) {
                alert(err)
             },
             complete: function (jqXHR, status) {
             }
    });
}
